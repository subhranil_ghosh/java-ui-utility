package com.src;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class HtmlEditorImpl {
	public static List<String> fileList = new ArrayList<>();
	public static void main(String[] args) {
		
		try {
			String parentFolder = "\\path\\to\\folder" ;
			List<String> htmlFilesList = listHtmlFilesFromFolder(new File(parentFolder));
			htmlFilesList.forEach(htmlFile -> updateHtmlDocs(htmlFile));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static List<String> listHtmlFilesFromFolder(final File folder) throws Exception {
		for (final File fileEntry : folder.listFiles()) {
	        if (fileEntry.isDirectory()) {
	        	listHtmlFilesFromFolder(fileEntry);
	        } else {
	        	String fileName = fileEntry.getCanonicalPath().trim();
	        	if(fileName != null && !fileName.isEmpty()){
	        		if(fileName.toLowerCase().endsWith(".html")){
	        			fileList.add(fileName);
	        		}
	        	}
	        }
	    }
		return fileList;
	}
	
	public static void updateHtmlDocs(final String fileName) {
		BufferedWriter  writer = null;
		try {
			File input = new File(fileName);
			/*String content = Jsoup.parse(input, "UTF-8").html();
			String modifiedContent = content.replaceAll("<button", "<button id=\"test\" ");
			FileUtils.writeStringToFile(input, modifiedContent, "UTF-8");*/
			//System.out.println(modifiedContent);
			/*Document doc = Jsoup.parse(modifiedContent);
			HTMLWriter wr = new HTMLWriter(new FileWriter(fileName), doc);
			wr.write();*/
			/*BufferedWriter  writer = new BufferedWriter(new FileWriter(fileName));
	        writer.write(modifiedContent);
	        writer.close();*/
			
			Document doc = Jsoup.parse(input, "UTF-8");
			Elements links = doc.select("button");
			//links.first().attr("id", "time");
			links.attr("id", "time");
			writer = new BufferedWriter(new FileWriter(fileName));
	        writer.write(doc.html());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(writer != null) {
				try {
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
